FROM wordpress
RUN cd /tmp \
&& apt-get update && apt-get install -y curl apt-utils wget unzip iproute2\
&& rm -rf /var/lib/apt/lists/*
RUN wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh --disable-telemetry --stable-channel --claim-token cHXnQ6EQsqNpISabIUuIHuqIvfAU-pUZixSmMBX7LFAl_vDahN53xR5gGNVoUPVSPhwqds9BGcYdZUex_Z-wse1AHkQr5u6g3vi3F4ZCAEUABcyO73nb8fiDcT0SZzFdlIPgf5o --claim-rooms 933f4539-5da0-45ed-947f-817ba6d04c6e 
EXPOSE 19999/tcp
